﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator_WU
{
    class Program
    {
        static void Main(string[] args)
        {
            // Central calculator instance - do only instanciate once to reuse internal state (result).
            Calculator calc = new Calculator();

            // Declare variable to read-in operation sign in do-while loop bellow.
            char operationSign;

            do
            {
                // Print instructions on how to use the calculator.
                Console.WriteLine("Choose the arithmetic operation or press [e] to exit:");
                Console.WriteLine($"  {Calculator.addSign} : Addition");
                Console.WriteLine($"  {Calculator.subSign} : Subtraction");
                Console.WriteLine($"  {Calculator.mulSign} : Multiplication");
                Console.WriteLine($"  {Calculator.divSign} : Division\n");

                do
                {
                    // Read the operation sign.
                    Console.Write("\nOperation: ");
                    operationSign = Console.ReadKey().KeyChar;

                    // If it's a valid operation sign, start reading operands.
                    if (calc.IsValidOperationSign(operationSign))
                    {
                        int[] operands = ReadOperands();

                        if (operands.Length > 0)
                        {
                            // You see a switch expression here. This syntax was introduced C# version 8.0.
                            var result = operands.Length switch
                            {
                                // Select the corresponding calculator method depending on the number of operands given.
                                // Note: This could be simlified drastically! Just use the last option for any number of operands.
                                1 => calc.Calculate(operationSign, operands[0]),
                                2 => calc.Calculate(operationSign, operands[0], operands[1]),
                                _ => calc.Calculate(operationSign, operands),
                            };

                            // Print the result followed by a separator line.
                            Console.WriteLine($"\n\nResult = {result}");
                            Console.WriteLine($"\n{string.Join(null, Enumerable.Repeat('-', 50).ToArray())}\n");

                            // Since we got a valid operator sign, we can BREAK out of the (inner) do-while loop now.
                            // Beware: Some say, the use of break keyword is discouraged outside of switch-case constructs!
                            // In this case, you could also sextend the corresponding while expression bellow.
                            break;
                        }
                    }
                } while (!calc.IsValidOperationSign(operationSign) && operationSign != 'E');

            } while (operationSign != 'E');
        }

        /// <summary>
        /// Reads a variable count of integer operands and returns them as an int array.
        /// The reading of operands is continued until an empty value is provided.
        /// Be aware: Entering invalid characters (as letters or special characters) results in an exception being thrown.
        /// To work arround this issue, one would have to know and use the concept of out parameters using the out keyword.
        /// Consider this as a teaser for future teaching in this module :-)
        /// </summary>
        /// <returns>Entered operand values</returns>
        public static int[] ReadOperands()
        {
            string input;
            char operandIndex = 'a';

            // Use a list (instead of an array) since we cannot know the number of operands to read in advance.
            List<int> operands = new List<int>();

            Console.WriteLine("\n\nGeben Sie ganzzahlige Werte für die Operation ein:\n");

            do
            {
                // Read (enumerated) operand values.
                Console.Write($"  {operandIndex++} = ");
                input = Console.ReadLine();

                // Add operand to list - if it's not empty.
                if (!string.IsNullOrWhiteSpace(input))
                {
                    operands.Add(int.Parse(input));
                }

            } while (!string.IsNullOrWhiteSpace(input));

            // The list is transformed into an array since the operands count is not expected to change and
            // arrays are easier to use regarding variable parameter count methods.
            return operands.ToArray();
        }
    }
}
