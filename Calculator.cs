﻿using System;

namespace Calculator_WU
{
    class Calculator
    {
        // Declare operator sign 
        public const char addSign = '+';
        public const char subSign = '-';
        public const char mulSign = '*';
        public const char divSign = '/';

        // Use this field to store results of calculations.
        private int result;

        #region TWO-PARAMETERS

        public int Calculate(char operationSign, int operandA, int operandB)
        {
            return operationSign switch
            {
                addSign => Add(operandA, operandB),
                subSign => Subtract(operandA, operandB),
                mulSign => Multiply(operandA, operandB),
                divSign => Divide(operandA, operandB),
                _ => throw ValidateOperationSign(operationSign, nameof(operationSign)),
            };

            throw new ArgumentException("Invalid operation given.");
        }

        public int Add(int a, int b)
        {
            result = a + b;
            return result;
        }

        public int Subtract(int a, int b)
        {
            result = a - b;
            return result;
        }

        public int Multiply(int a, int b)
        {
            result = a * b;
            return result;
        }

        public int Divide(int a, int b)
        {
            result = a / b;
            return result;
        }

        #endregion TWO-PARAMETERS


        #region ONE-PARAMETER

        public int Calculate(char operationSign, int operand)
        {
            return operationSign switch
            {
                addSign => Add(operand),
                subSign => Subtract(operand),
                mulSign => Multiply(operand),
                divSign => Divide(operand),
                _ => throw ValidateOperationSign(operationSign, nameof(operationSign)),
            };

            throw new ArgumentException("Invalid operation given.");
        }

        public int Add(int a)
        {
            result += a;
            return result;
        }

        public int Subtract(int a)
        {
            result -= a;
            return result;
        }

        public int Multiply(int a)
        {
            result *= a;
            return result;
        }

        public int Divide(int a)
        {
            result /= a;
            return result;
        }

        #endregion ONE-PARAMETER


        #region MULTIPLE-PARAMETERS

        public int Calculate(char operation, params int[] operands)
        {
            return operation switch
            {
                addSign => Add(operands),
                subSign => Subtract(operands),
                mulSign => Multiply(operands),
                divSign => Divide(operands),
                _ => throw ValidateOperationSign(operation, nameof(operation)),
            };
        }

        public int Add(params int[] operands)
        {
            int sum = operands.Length > 1 ? 0 : result;
            foreach (int summands in operands)
            {
                sum += summands;
            }
            return result = sum;
        }

        public int Subtract(params int[] operands)
        {
            int difference = operands.Length > 1 ? operands[0] : result;
            int startingIndex = operands.Length > 1 ? 1 : 0;
            for (int operandIndex = startingIndex; operandIndex < operands.Length; operandIndex++)
            {
                difference -= operands[operandIndex];
            }
            return result = difference;
        }

        public int Multiply(params int[] operands)
        {
            int product = operands.Length > 1 ? 1 : result;
            foreach (int factor in operands)
            {
                product *= factor;
            }
            return result = product;
        }

        public int Divide(params int[] operands)
        {
            int quotient = operands.Length > 1 ? operands[0] : result;
            int startingIndex = operands.Length > 1 ? 1 : 0;
            for (int operandIndex = startingIndex; operandIndex < operands.Length; operandIndex++)
            {
                quotient /= operands[operandIndex];
            }
            return result = quotient;
        }

        #endregion MULTIPLE-PARAMETERS

        public bool IsValidOperationSign(char operationSign)
        {
            return operationSign == addSign
                || operationSign == subSign
                || operationSign == mulSign
                || operationSign == divSign;
        }

        private ArgumentException ValidateOperationSign(char operationSign, string parameterName)
        {
            if (!IsValidOperationSign(operationSign))
            {
                return new ArgumentException($"Invalid operator sign given: '{operationSign}'", parameterName);
            }

            return null;
        }
    }
}